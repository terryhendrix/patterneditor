/**
 * Created by terry on 15-2-14.
 */
var DEBUG = false;
var pattern = angular.module('patternEditor', []);

pattern.run(function($rootScope){
    console.info("App loading");

    $rootScope.uuid = function() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
});

pattern.service("HttpService", function($http, $q){
    var self =  {
        get: function(url, params) {
            if(typeof params === "undefined")
                params = {};

            return $q(function(success, failure) {
                $http.get(url, {params:params}).success(function(result) {
                    if(DEBUG) console.log(result);
                    success(result);
                }).error(function(reason) {
                    console.error(reason);
                    failure(reason);
                });
            });
        },
        put: function(url, body) {
            return $q(function(success, failure) {
                $http.put(url, body).success(function(result) {
                    if(DEBUG) console.log(result);
                    success(result);
                }).error(function(reason) {
                    console.error(reason);
                    failure(reason);
                });
            });
        },
        post: function(url, body) {
            return $q(function(success, failure) {
                $http.post(url, body).success(function(result) {
                    if(DEBUG) console.log(result);
                    success(result);
                }).error(function(reason) {
                    console.error(reason);
                    failure(reason);
                });
            });
        },
        del: function(url) {
            return $q(function(success, failure) {
                $http.delete(url).success(function(result) {
                    if(DEBUG) console.log(result);
                    success(result);
                }).error(function(reason) {
                    console.error(reason);
                    failure(reason);
                });
            });
        }
    };

    return self;
});

pattern.service('Api', function(HttpService, $q, $timeout) {
    var model = undefined;
    var self = {
        loadModel: function() {
            return $q(function(success, failure) {
                HttpService.get("/service/model").then(function(_model) {
                    model = _model;
                    success(model);
                });
            });
        },
        getById: function(id) {
            return HttpService.get("/service/model/"+id)
        },
        saveCategory: function(category) {
                      return HttpService.put("/service/model/category", category);
        },
        deleteCategory: function(categoryId) {
            return HttpService.del("/service/model/category/"+categoryId);
        },
        savePattern: function(pattern) {
            return HttpService.put("/service/model/pattern", pattern);
        },
        deletePattern: function(patternId) {
            return HttpService.del("/service/model/pattern/"+patternId);
        },
        downloadModel: function() {
            return HttpService.get("/service/model/export")
        },
        uploadModel: function(model) {
            return HttpService.put("/service/model/import", model)
        },
        getModel: function(){
            return model;
        }
    };

    self.loadModel();

    return self;
});

pattern.controller('PatternController', function($scope, Api, $timeout)
{
    console.info("PatternController loading");
    $scope.categories = [];
    $scope.stack = [];
    $scope.focus = {};

    // give form variables a value
    $scope.resetCatForm = function() {
        console.log("Resetting cat form inputs");
        $scope.newCategoryName = "";
        $scope.newCategoryDescription = "";
    };

    $scope.resetPatternForm = function() {
        // KEEP THIS: Fixing angular bug
        var stack = $scope.stack;
        $scope.stack = [];
        $timeout(function(){
            $scope.newPatternName = "";
            $scope.newPatternDescription = "";
            $scope.newPatternContext = "";
            $scope.newPatternProblem = "";
            $scope.newPatternSolution = "";
            $scope.newPatternImageUrl = "http://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg";
            $scope.newPatternConsequences = "";
            $scope.stack = stack;
        }, 0)
    };

    $scope.resetPatternForm();
    $scope.resetCatForm();

    $scope.$watch(Api.getModel, function(model) {
        if(typeof model !== "undefined" && $scope.stack.length == 0) {
            $scope.openCategory(model)
        }
    });

    $scope.openCategory = function(category) {
        console.info("Opening "+category.name+" "+category.id);

        Api.getById(category.id).then(function(node){
            $scope.focus = node;
            $scope.forward($scope.focus);
        });
    };

    $scope.forward = function(node) {
        if(!$scope.existsInStack(node)) {
            $scope.stack.push(node);
        }
    };

    $scope.existsInStack = function(node){
        return $scope.stack.filter(function(_node){
            return node.id == _node.id;
        }).length != 0;
    };


    $scope.back = function(){
        if($scope.stack.length > 1) {
            $scope.stack.splice($scope.stack.length-1, 1);
            $scope.openCategory($scope.stack[$scope.stack.length-1])
        }
    };

    $scope.showPattern = function(pattern) {
        $scope.pattern = pattern;
        $scope.editPattern = false;
        $("#modalPattern").modal("show");
    };

    $scope.showImport= function() {
            $("#modalImport").modal("show");
        };


    $scope.startEdit = function() {
        $scope.editPattern = true;
    };

    $scope.savePattern = function(newPatternName, newPatternDescription, newPatternContext, newPatternProblem, newPatternSolution, newPatternImageUrl, newPatternConsequences) {
        var pattern = {
            id:$scope.uuid(),
            name:newPatternName,
            description:newPatternDescription,
            context:newPatternContext,
            problem:newPatternProblem,
            solution:newPatternSolution,
            imageUrl:newPatternImageUrl,
            consequences : newPatternConsequences
        };
        console.info("Adding");
        console.info(pattern);
        $scope.resetPatternForm();
        $scope.focus.patterns.push(pattern);
        Api.saveCategory($scope.focus);
    };

    $scope.saveCategory = function(newPatternName, newPatternDescription) {
        $scope.focus.categories.push({
            id:$scope.uuid(),
            name:newPatternName,
            description:newPatternDescription,
            patterns: [],
            categories: []
        });
        $scope.resetCatForm();
        Api.saveCategory($scope.focus)
    };

    $scope.deletePattern = function(pattern) {
        $scope.focus.patterns = $scope.focus.patterns.filter(function(_pattern){ return _pattern.id != pattern.id});
        Api.deletePattern(pattern.id);
    };

    $scope.uploadFile = function(){
            Api.uploadModel($scope.importJson).then(function(rs) {
                $("#modalImport").modal("hide");
                $timeout(function(){
                    location.reload();
                }, 10)
            });
        };

    $scope.deleteCategory = function(category) {
        $scope.focus.categories = $scope.focus.categories.filter(function(_category){ return _category.id != category.id});
        Api.deleteCategory(category.id);
    };

    $scope.updatePattern = function(pattern) {
        Api.savePattern(pattern);
    };

    $scope.file_changed = function(element) {
            var photofile = element.files[0];
            console.log(photofile);
            var reader = new FileReader();
            reader.onload = function(result){
                console.info("Read json from file");
                $scope.importJson = result.currentTarget.result;
            };
            reader.readAsBinaryString(photofile);
    };

    $scope.show = true;
});

