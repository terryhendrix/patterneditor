package nl.jan.terry.hu.util
import spray.json._
import spray.json.{JsString, RootJsonFormat, JsValue, DefaultJsonProtocol}
import nl.jan.terry.hu.domain._
import nl.jan.terry.hu.api._



trait JsonMarshallers extends DefaultJsonProtocol
{
  implicit def jsValueToString(js:JsValue) = js.asInstanceOf[JsString].value
  implicit def jsValueToCatsSeq(js:JsValue):Seq[Category] = js.asInstanceOf[JsArray].elements.map(_.convertTo[Category])
  implicit def jsValueToPatternsSeq(js:JsValue):Seq[Pattern] = js.asInstanceOf[JsArray].elements.map(_.convertTo[Pattern])

  implicit val errorFormat = jsonFormat2(Error)
  implicit val patternFormat = jsonFormat8(Pattern)

  implicit object CategoriesFormat extends RootJsonFormat[Category]
  {
    def read(json: JsValue):Category = {
      val fields = json.asJsObject.fields
      Category(
        id = fields("id"),
        name = fields("name"),
        description = fields("description"),
        patterns = fields("patterns"),
        categories = fields("categories")
      )
    }

    def write(c: Category) = {
      val subs = c.categories.toJson
      val patterns = c.patterns.toJson
      JsObject(List[JsField](
        "id" -> JsString(c.id),
        "name" -> JsString(c.name),
        "description" -> JsString(c.description),
        "categories" -> subs,
        "patterns" -> patterns
      ))
    }
  }

  implicit val rootFormat = jsonFormat3(Root)

  implicit class As(json:String) {
    def asObject[U:JsonReader] = json.parseJson.convertTo[U]
  }

  implicit class AsJson[U](u:U) {
    def asJson()(implicit writer: JsonWriter[U]) = u.toJson.prettyPrint
  }
}
