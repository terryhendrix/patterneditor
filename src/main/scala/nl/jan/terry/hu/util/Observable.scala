package nl.jan.terry.hu.util

import akka.actor.ActorRef

/**
 * Created by terryhendrix on 02/04/15.
 */
trait Observable[ObservableType] {
  def subscribe(observer:Observer[ObservableType])
}

trait Observer[ObservableType] {
  def notify(u:ObservableType):Unit
}