package nl.jan.terry.hu.util

import java.util.UUID

/**
 * Created by terryhendrix on 02/04/15.
 */
object Convert {
  implicit def uuidToString(u:UUID):String = u.toString
  implicit def genericToOption[U](u:U):Option[U] = Option(u)
}