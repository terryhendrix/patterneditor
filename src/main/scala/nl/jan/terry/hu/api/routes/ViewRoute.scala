package nl.jan.terry.hu.api.routes
import scala.io.Source
import nl.jan.terry.hu.api.DeveloperMode

/**
 * Created by JanO on 20-3-2015.
 */
trait ViewRoute extends SimpleRoute with DeveloperMode
{
  def viewRoute =
    pathSingleSlash {
      getResource("web/index.html")
    } ~ getResourceFromDir("web")
}
