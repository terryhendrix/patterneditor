package nl.jan.terry.hu.api.routes
import spray.http._
import spray.json._
import scala.util.Try
import nl.jan.terry.hu.util.JsonMarshallers
import nl.jan.terry.hu.domain.{Node, Category, Pattern, Root}
import nl.jan.terry.hu.service.PatternService
import spray.httpx.marshalling.ToResponseMarshallable
import spray.httpx.unmarshalling.FromRequestUnmarshaller
import nl.jan.terry.hu.api.Errors
import nl.jan.terry.hu.domain.Pattern
import nl.jan.terry.hu.domain.Category

/**
 * The controller route hosts several resources.
 * GET /service/model          =>  returns the current model in JsonFormat
 * PUT /service/model/pattern  =>  replaces a pattern by id
 * PUT /service/model/category =>  replaces a category, new patterns must be added by submitting its parent category, including the new pattern
 * GET /service/model/export   =>  Makes the browser download a file with the current state
 * GET /service/model/import   =>  Imports the model
 */
trait ControllerRoute extends SimpleRoute with JsonMarshallers
{
  def service:PatternService

  def controllerRoute = pathPrefix("service") {
    path("model") {
      get {
        complete {
          service.getRoot.asJson
        }
      }
    } ~
    path ("model" / "import") {
      (put | post) {
        entity(as[String]) { json =>
          complete {
            Try(json.parseJson).map[ToResponseMarshallable] { _ =>
              service.importJson(json)
              StatusCodes.NoContent
            }.recover[ToResponseMarshallable] {
              case ex:Throwable => StatusCodes.InternalServerError -> Errors.from(ex).asJson
            }
          }
        }
      }
    } ~
    path("model" / "export") {
      get {
        respondWithMediaType(MediaTypes.`application/octet-stream`) { // tell browser to save file
          respondWithHeader(HttpHeaders.`Content-Disposition`(s"""inline; filename="${service.exportJson().fileName}" """)) {  // tell browser the filename = export.json
            complete {
              service.exportJson().body
            }
          }
        }
      }
    } ~
    path("model" / Segment) { id =>
      get {
        complete {
          service.getRoot.find(id) match {
            case Some(root:Root)          => root.asJson
            case Some(category:Category)  => category.asJson
            case Some(pattern:Pattern)    => pattern.asJson
            case _                        => StatusCodes.InternalServerError -> Errors.from(new Exception(s"Could not find id $id")).asJson
          }
        }
      }
    } ~
    path("model" / "category" / Segment) { id =>
      (delete) {
        complete {
          service.getRoot.find(id) match {
            case Some(_:Category) =>
              service.delete(id)
              StatusCodes.NoContent
            case Some(_) =>
              StatusCodes.InternalServerError -> Errors.from(new Exception(s"$id is not a category.")).asJson()
            case None =>
              StatusCodes.InternalServerError -> Errors.from(new Exception(s"$id not found.")).asJson()
          }
        }
      }
    } ~
    path("model" / "category") {
      (put | post) {
        entity(as[String]) { json =>
          complete {
            Try(json.asObject[Category]).recoverWith { case _ =>
              Try(json.asObject[Root])
            }.map[ToResponseMarshallable] { node =>
              service.setNode(node)
              StatusCodes.NoContent
            }.recover[ToResponseMarshallable] {
                case ex:Throwable => StatusCodes.InternalServerError -> Errors.from(ex).asJson
            }
          }
        }
      }
    } ~
    path("model" / "pattern" / Segment) { id =>
      (delete) {
        complete {
          service.getRoot.find(id) match {
            case Some(_:Pattern) =>
              service.delete(id)
              StatusCodes.NoContent
            case Some(_) =>
              StatusCodes.InternalServerError -> Errors.from(new Exception(s"$id is not a patern.")).asJson()
            case None =>
              StatusCodes.InternalServerError -> Errors.from(new Exception(s"$id not found.")).asJson()
          }
        }
      }
    } ~
    path("model" / "pattern") {
      (post | put) {
        entity(as[String]) { json =>
          complete{
            Try(json.asObject[Pattern]).map[ToResponseMarshallable] { pattern =>
              service.setNode(pattern)
              StatusCodes.NoContent
            }.recover[ToResponseMarshallable] {
              case ex:Throwable => StatusCodes.InternalServerError -> Errors.from(ex).asJson
            }
          }
        }
      }
    }
  }
}
