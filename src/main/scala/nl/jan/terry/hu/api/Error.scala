package nl.jan.terry.hu.api


object Errors {
  def from(ex:Throwable) = Error(ex.getMessage, Option(ex.getCause).map(_.getMessage))
}

case class Error(message:String, cause:Option[String])