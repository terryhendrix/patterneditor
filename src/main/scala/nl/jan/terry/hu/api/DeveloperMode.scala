package nl.jan.terry.hu.api

import scala.io.Source
import spray.routing.Directives
import akka.actor.ActorSystem
import nl.jan.terry.hu.api.routes.SimpleRoute

/**
 * Created by terryhendrix on 04/04/15.
 */
trait DeveloperMode { this:SimpleRoute =>
  def system:ActorSystem
  def developerMode = system.settings.config.getConfig("web").getBoolean("developer.mode")
  def pathToResources = system.settings.config.getConfig("web").getString("path.to.resources")

  def getResourceFromDir(path: String) = {
    if (developerMode) {
      getFromDirectory(pathToResources + path)
    }
    else {
      getFromResourceDirectory(path)
    }
  }

  def getResource(path:String) = {
    if(developerMode) {
      getFromDirectory(pathToResources + path)
    }
    else {
      getFromResource(path)
    }
  }

  def getResourceAsString(path:String) = {
    if(developerMode)
      Source.fromFile(pathToResources + path).mkString
    else
      Source.fromURL(getClass.getResource("/"+path)).mkString
  }
}
