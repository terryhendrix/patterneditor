package nl.jan.terry.hu.api.routes

import akka.actor.{ActorSystem, ActorRefFactory}
import akka.event.LoggingAdapter
import akka.util.Timeout
import com.typesafe.config.Config
import spray.routing.Directives

import scala.concurrent.ExecutionContext
import nl.jan.terry.hu.util.JsonMarshallers

/**
 * Created by JanO on 20-3-2015.
 */
trait SimpleRoute extends Directives with JsonMarshallers
{
  implicit def ec:ExecutionContext
  implicit def actorRefFactory:ActorRefFactory
  implicit def timeout:Timeout
  implicit def log:LoggingAdapter
  def config:Config
}
