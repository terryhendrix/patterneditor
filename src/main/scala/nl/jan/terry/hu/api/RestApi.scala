package nl.jan.terry.hu.api

import akka.actor.ActorSystem
import nl.jan.terry.hu.api.routes.{ControllerRoute, ViewRoute}
import spray.json._
import spray.routing.SimpleRoutingApp
import nl.jan.terry.hu.domain.Pattern

trait RestApi extends  ViewRoute with ControllerRoute with SimpleRoutingApp
{
  implicit def system:ActorSystem
  def pageParameters = parameters('from ? 0, 'size ? 10)

  def routeDirectives = viewRoute ~ controllerRoute
}

