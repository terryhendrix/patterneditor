package nl.jan.terry.hu.service
import spray.json._
import nl.jan.terry.hu.domain._
import nl.jan.terry.hu.repository.Repository
import nl.jan.terry.hu.util.{JsonMarshallers, Observable, Observer, Convert}
import java.util.UUID
import nl.jan.terry.hu.domain
import nl.jan.terry.hu.exporter.{Export, JsonExporter}
import scala.util.Try
import akka.event.LoggingAdapter


// Companion object holding "statics"
// The object PatternService will be compiled to PatternService$.class, and acts like a singleton


// The class PatternService will be compiled to PatternService.class
// This is the way a class differs from an object
class PatternService(repository:Repository with Observable[Root])(implicit log:LoggingAdapter) extends Observer[Root]
{
  import Convert.genericToOption
  val exporter = new JsonExporter(repository)
  var state:Option[Root] = None

  repository.subscribe(this)

  def getRoot:Root = {
    state.getOrElse {
      repository.importModel(InitialModel.root)
      InitialModel.root
    }
  }

  def setNode(node:Node) = repository.save(node)

  def delete(id:String) = {
    for {
      state <- state
      parent <- state.parent(id)
      saveThis <- parent.delete(id)
    } {
      log.info(s"Deleting $id")
      repository.save(saveThis)
    }
  }

  def importJson(json:String) = {
    log.info("Handling json-import")
    exporter.doImport(json)
  }

  def exportJson():Export = {
    log.info("Handling json-export")
    exporter.doExport(state.getOrElse(InitialModel.root))
  }

  override def notify(updated: Root): Unit = {
    log.info(s"Notified of new state.")
    state = updated
  }
}
