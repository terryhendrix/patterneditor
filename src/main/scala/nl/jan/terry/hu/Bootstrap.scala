package nl.jan.terry.hu
import akka.actor.ActorSystem
import akka.util.Timeout
import com.typesafe.config.Config
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import nl.jan.terry.hu.api.RestApi
import nl.jan.terry.hu.service.PatternService
import nl.jan.terry.hu.repository.{Repository, EventSourcedRepository}
import nl.jan.terry.hu.domain.{Root, Category, Pattern}
import java.util.UUID
import nl.jan.terry.hu.util.{Observable, Convert}
import akka.event.LoggingAdapter

object Bootstrap extends App with RestApi {
  override implicit val system: ActorSystem = ActorSystem("Patterns")
  override implicit def log: LoggingAdapter = system.log
  override implicit val ec: ExecutionContext = system.dispatcher
  override val config: Config = system.settings.config
  override implicit val timeout: Timeout = Timeout(5 seconds)

  val repository:Repository with Observable[Root] = EventSourcedRepository(system)
  val service = new PatternService(repository)

  startServer(interface = "0.0.0.0", port = 8080) { routeDirectives }
}