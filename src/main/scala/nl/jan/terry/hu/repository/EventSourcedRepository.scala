package nl.jan.terry.hu.repository
import akka.actor.{ActorLogging, ActorSystem, Props}
import akka.persistence.{PersistentActor, RecoveryFailure}
import nl.jan.terry.hu.domain.{Node, Root}
import nl.jan.terry.hu.util.{Convert, Observable, Observer}

// Companion object to hold "static" / importable references
object EventSourcedRepository {
  val name = "Repository"
  case class Save[U <: Node](node:U)
  case class Import(root:Root)
  case class Subscribe(observer:Observer[Root])

  // factory method
  def apply(implicit system:ActorSystem) = {
    new EventSourcedRepository()
  }
}

class EventSourcedRepository(implicit system:ActorSystem) extends Repository with Observable[Root]
{
  import Convert._
  import EventSourcedRepository._

  private val actor = system.actorOf(Props(new RepositoryActor),name)

  override def save(node:Node) = {
    actor ! Save[node.type](node)
  }

  override def importModel(root:Root) = {
    actor ! Import(root)
  }

  override def subscribe(observer: Observer[Root]): Unit = {
    actor ! Subscribe(observer)
  }

  private class RepositoryActor extends PersistentActor with ActorLogging
  {
    override def persistenceId: String = name
    var state:Option[Root] = None
    var observers:Seq[Observer[Root]] = Seq.empty

    override def receiveCommand: Receive = {
      case event:Save[_]   => persist(event)(handleSave)
      case event:Import    => persist(event)(handleImport)
      case event:Subscribe => subscribe(event)
    }

    override def receiveRecover: Receive = {
      case event:Save[_]   => handleSave(event)
      case event:Import => handleImport(event)
      case msg:RecoveryFailure =>
        log.error(s"Failed to recover", msg.cause)
        msg.cause.printStackTrace()
    }

    def subscribe(event:Subscribe) = {
      state.map(root => event.observer.notify(root) )
      observers = observers :+ event.observer
    }

    def handleSave(save:Save[_ <: Node]):Unit = {
      log.info("Saving")
      state = state.map(_.replace(save.node))
      notifyObservers()
    }

    def handleImport(_import:Import):Unit = {
      log.info("Importing")
      state = _import.root
      notifyObservers()
    }

    def notifyObservers() = {
      state.map(root => observers.map(_.notify(root)))
    }
  }
}
