package nl.jan.terry.hu.repository

import nl.jan.terry.hu.domain.{Root, Node}
import nl.jan.terry.hu.util.Observable

trait Repository {
  def save(node:Node):Unit
  def importModel(root:Root):Unit
}