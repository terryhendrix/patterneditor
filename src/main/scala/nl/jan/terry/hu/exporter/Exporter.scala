package nl.jan.terry.hu.exporter
import nl.jan.terry.hu.repository.Repository
import akka.event.LoggingAdapter

case class Export(fileName:String,body:String)

trait Serializer[U] {
  def serialize(u:U):String
}

trait Deserializer[U] {
  def deserialize(body:String):U
}

trait Exporter[U] extends Serializer[U] with Deserializer[U]
{
  def fileName:String
  def repo:Repository
  def log:LoggingAdapter

  def doExport(u:U) = Export(fileName, serialize(u))

  def doImport(body:String):Unit
}