package nl.jan.terry.hu.exporter

import nl.jan.terry.hu.domain.Root
import nl.jan.terry.hu.util.JsonMarshallers
import nl.jan.terry.hu.repository.Repository
import akka.event.LoggingAdapter

class JsonExporter(override val repo:Repository)(implicit override val log:LoggingAdapter) extends Exporter[Root] with JsonMarshallers
{
  override val fileName = "export.json"

  override def deserialize(body: String): Root = body.asObject[Root]

  override def serialize(u: Root): String = u.asJson()

  override def doImport(body:String):Unit  = repo.importModel(deserialize(body))
}
