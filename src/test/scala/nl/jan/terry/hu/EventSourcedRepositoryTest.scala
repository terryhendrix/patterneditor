package nl.jan.terry.hu
import scala.concurrent.duration._
import nl.jan.terry.hu.domain.{Root, Pattern, Category, Node}
import java.util.UUID
import nl.jan.terry.hu.util.{Observer, Convert}
import nl.jan.terry.hu.spec.TestSpec
import akka.testkit.TestProbe

/**
 * Created by terryhendrix on 02/04/15.
 */
class EventSourcedRepositoryTest extends TestSpec
{
  import Convert.uuidToString
  val testProbe = TestProbe()
  val testProbe2 = TestProbe()
  val emptyRoot = Root("emptyRoot")

  val observer = new Observer[Root] {
    override def notify(root: Root): Unit = {
      testProbe.ref ! root
    }
  }

  val observer2 = new Observer[Root] {
    override def notify(root: Root): Unit = {
      testProbe2.ref ! root
    }
  }

  "The repo" should "be subscribed to" in {
    repo.importModel(root)
    repo.subscribe(observer)
    testProbe.receiveOne(5 seconds) match {
      case `root` =>  log.info(s"Got expected $root")
      case other     => fail(s"Got:\n$other\nExpected:\n$root")
    }
  }

  it should " import" in {
    repo.importModel(root)
    testProbe.expectMsg(root)
  }

  val newClassScope = classScope.copy(name = "NEW", description = "NEW")
  val expectedNewRoot = root.replace(newClassScope)

  it should "save a node" in {
    repo.save(newClassScope)
    testProbe.expectMsg(expectedNewRoot)
  }

  it should "handle another subscriber" in {
    repo.subscribe(observer2)
    testProbe2.expectMsg(expectedNewRoot)
  }

  it should "handle 2 observers when saving" in {
    repo.save(emptyRoot)
    testProbe.expectMsg(emptyRoot)
    testProbe2.expectMsg(emptyRoot)
  }

  it should "handle 2 observers when importing" in {
    repo.save(root)
    testProbe.expectMsg(root)
    testProbe2.expectMsg(root)
  }
}
