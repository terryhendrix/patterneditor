package nl.jan.terry.hu
import spray.json._
import nl.jan.terry.hu.spec.TestSpec
import nl.jan.terry.hu.util.JsonMarshallers
import nl.jan.terry.hu.domain.Root

/**
 * Created by terryhendrix on 04/04/15.
 */
class JsonMarshallersTest extends TestSpec with JsonMarshallers
{
  "The JsonMarshallers" should "marshal and unmarshall the model to and from json" in {
    val createdJson = root.toJson.prettyPrint
    log.info(s"$createdJson")
    val reverseRoot = createdJson.parseJson.convertTo[Root]
    log.info(s"$reverseRoot")
    assert(reverseRoot == root)
  }
}
