package nl.jan.terry.hu

import nl.jan.terry.hu.domain.{Root, Pattern, Category, Node}
import java.util.UUID
import nl.jan.terry.hu.util.Convert
import nl.jan.terry.hu.spec.TestSpec

/**
 * Created by terryhendrix on 02/04/15.
 */
class NodesTest extends TestSpec
{
  import Convert.uuidToString

  "A Node" should "be a category" in {
    val category = Category(UUID.randomUUID(), "Class scope", "Category for class patterns")
    category match {
      case _:Node => log.info("Category is a Node")
      case _ => fail("Category should be Node")
    }
  }

  it should "be a pattern" in {
    val pattern = Pattern(UUID.randomUUID(), "Chain of responsibility", "many handlers", "Cannot create reference", "a","b",None,"")
    pattern match {
      case _:Node => log.info("Pattern is a Node")
      case _ => fail("Pattern should be Node")
    }
  }

  "A Category" should "have patterns" in {
    val cor = Pattern(UUID.randomUUID(), "Chain of responsibility", "many handlers", "Cannot create reference", "a","b",None,"")
    val factory = Pattern(UUID.randomUUID(), "Chain of responsibility", "many handlers", "Cannot create reference", "a","b",None,"")
    val creational = Category(UUID.randomUUID(), "Creational", "Creational class patterns", patterns = cor :: factory :: Nil)
    assert(creational.patterns.nonEmpty, "Should have patterns")
  }

  "A Node" should "be searchable" in {
    assert(root.find(cor.id) == Some(cor), "Should have found the Cor pattern")
    assert(root.find(factory.id) == Some(factory), "Should have found the Factory pattern")
    assert(root.find(creational.id) == Some(creational), "Should have found the Creational category")
    assert(root.find(structural.id) == Some(structural), "Should have found the Structural category")
    assert(root.find(behavioural.id) == Some(behavioural), "Should have found the Behavioural category")
    assert(root.find(classScope.id) == Some(classScope), "Should have found the Class Scope category")
    assert(root.find(objectScope.id) == Some(objectScope), "Should have found the Object Scope category")
    assert(root.find(UUID.randomUUID()) == None, "Non existing UUID's should not be found")
  }

  it should "determine if it contains another Node" in {
    assert(root.contains(cor.id), "Should contain the Cor pattern")
    assert(root.contains(factory.id), "Should contain the Factory pattern")
    assert(root.contains(creational.id), "Should contain the Creational category")
    assert(root.contains(structural.id), "Should contain the Structural category")
    assert(root.contains(behavioural.id), "Should contain the Behavioural category")
    assert(root.contains(classScope.id), "Should contain the Class Scope category")
    assert(root.contains(objectScope.id), "Should contain the Object Scope category")
    assert(!root.contains(UUID.randomUUID()), "Contain should return false with non-existing UUID's")
  }

  it should "determine the parent" in {
    assert(root.parent(cor.id) == Some(creational), "Parent of COR should be Creational")
    assert(root.parent(factory.id) == Some(creational), "Parent of Factory should be Creational")
    assert(root.parent(creational.id) == Some(classScope), "Parent of Creational should be Class Scope")
    assert(root.parent(structural.id) == Some(classScope), "Parent of Structural should be Class Scope")
    assert(root.parent(behavioural.id) == Some(classScope), "Parent of Behavioural should be Class Scope")
    assert(root.parent(classScope.id) == Some(root), "Parent of ClassScope should be Root")
  }

  it should " update Root properly" in {
    val newRoot = Root(UUID.randomUUID())
    assert(root.replace(newRoot) == newRoot, "Updating Root should result in a new Root")
  }

  it should "update a first degree category properly" in {
    val newClassScope = classScope.copy(name = "NEW", description = "NEW")
    val expectedRoot = root.copy(categories = objectScope :: newClassScope :: Nil)

    assert(root.replace(newClassScope) == expectedRoot)
  }

  it should "update a second degree category properly" in {
    val newCreational = creational.copy(name = "NEW", description = "NEW")
    val newClassScope = classScope.copy(categories = newCreational :: behavioural :: structural :: Nil)
    val expectedRoot = root.copy(categories = objectScope :: newClassScope :: Nil)

    assert(root.replace(newCreational) == expectedRoot)
    assert(root.replace(newClassScope) == expectedRoot)
  }

  it should "update a pattern properly" in {
    val newFactory = factory.copy(name = "NEW", description = "NEW")
    val newCreational = creational.copy(patterns = cor :: newFactory :: Nil)
    val newClassScope = classScope.copy(categories = newCreational :: behavioural :: structural :: Nil)
    val expectedRoot = root.copy(categories = objectScope :: newClassScope :: Nil)

    assert(root.replace(newFactory) == expectedRoot)
    assert(root.replace(newCreational) == expectedRoot)
    assert(root.replace(newClassScope) == expectedRoot)
  }

  it should "delete pattern" in {
    assert(creational.contains(cor.id), "Should contain the Cor pattern")
    assert(creational.contains(factory.id), "Should contain the Factory pattern")

    assert(classScope.contains(creational.id), "Should contain the Creational category")
    assert(classScope.contains(structural.id), "Should contain the Structural category")
    assert(classScope.contains(behavioural.id), "Should contain the Behavioural category")

    assert(root.contains(classScope.id), "Should contain the Class Scope category")

    // return None when deleting self, this is the building block of the delete-in-tree algorithm
    assert(factory.delete(factory.id) == None)
    assert(classScope.delete(classScope.id) == None)
    assert(root.delete(root.id) == None)

    // delete pattern from a category
    val creationalWithoutCor = creational.delete(cor.id).get
    assert(creationalWithoutCor.contains(factory.id), s"Factory should still be there. Given $creationalWithoutCor")
    assert(!creationalWithoutCor.contains(cor.id), s"Cor should be deleted. Given $creationalWithoutCor")

    // delete pattern from 2nd degree category
    val classScopeWithoutCor = classScope.delete(cor.id).get
    assert(!classScopeWithoutCor.contains(cor.id), s"Should contain the Cor pattern Given. $classScopeWithoutCor")
    assert(classScopeWithoutCor.contains(factory.id), s"Should contain the Factory pattern. Given $classScopeWithoutCor")
    assert(classScopeWithoutCor.contains(creational.id), s"Should contain the Creational category. Given $classScopeWithoutCor")
    assert(classScopeWithoutCor.contains(structural.id), s"Should contain the Structural category. Given $classScopeWithoutCor")
    assert(classScopeWithoutCor.contains(behavioural.id), s"Should contain the Behavioural category. Given $classScopeWithoutCor")

    // delete pattern from 3rd degree category
    val rootWithoutCor = root.delete(cor.id).get
    assert(!rootWithoutCor.contains(cor.id), s"Should contain the Cor pattern. Given $rootWithoutCor")
    assert(rootWithoutCor.contains(factory.id), s"Should contain the Factory pattern. Given $rootWithoutCor")
    assert(rootWithoutCor.contains(creational.id), s"Should contain the Creational category. Given $rootWithoutCor")
    assert(rootWithoutCor.contains(structural.id), s"Should contain the Structural category. Given $rootWithoutCor")
    assert(rootWithoutCor.contains(behavioural.id), s"Should contain the Behavioural category. Given $rootWithoutCor")
    assert(rootWithoutCor.contains(objectScope.id), s"Should contain the Object Scope category. Given $rootWithoutCor ")
    assert(rootWithoutCor.contains(classScope.id), s"Should contain the Class Scope category. Given $rootWithoutCor")

    // delete a category and its pattern from a first degree category
    val classScopeWithoutCreational = classScope.delete(creational.id).get
    assert(!classScopeWithoutCreational.contains(cor.id), s"Should contain the Cor pattern. Given $classScopeWithoutCreational")
    assert(!classScopeWithoutCreational.contains(factory.id), s"Should contain the Factory pattern. Given $classScopeWithoutCreational")
    assert(!classScopeWithoutCreational.contains(creational.id), s"Should contain the Creational category. Given $classScopeWithoutCreational")
    assert(classScopeWithoutCreational.contains(structural.id), s"Should contain the Structural category. Given $classScopeWithoutCreational")
    assert(classScopeWithoutCreational.contains(behavioural.id), s"Should contain the Behavioural category. Given $classScopeWithoutCreational")

    // delete a category and its pattern from a second degree category
    val rootWithoutCreational = root.delete(creational.id).get
    assert(!rootWithoutCreational.contains(cor.id), s"Should contain the Cor pattern Given $rootWithoutCreational")
    assert(!rootWithoutCreational.contains(factory.id), s"Should contain the Factory pattern Given $rootWithoutCreational")
    assert(!rootWithoutCreational.contains(creational.id), s"Should contain the Creational category Given $rootWithoutCreational")
    assert(rootWithoutCreational.contains(structural.id), s"Should contain the Structural category Given $rootWithoutCreational")
    assert(rootWithoutCreational.contains(behavioural.id), s"Should contain the Behavioural category Given $rootWithoutCreational")
    assert(rootWithoutCreational.contains(classScope.id), s"Should contain the Class Scope category. Given $rootWithoutCreational")
    assert(rootWithoutCreational.contains(objectScope.id), s"Should contain the Object Scope category. Given $rootWithoutCreational")

    // delete a category and its children from a first degree category
    val rootWithoutClassScope = root.delete(classScope.id).get
    assert(!rootWithoutClassScope.contains(cor.id), s"Should contain the Cor pattern Given $rootWithoutClassScope")
    assert(!rootWithoutClassScope.contains(factory.id), s"Should contain the Factory pattern Given $rootWithoutClassScope")
    assert(!rootWithoutClassScope.contains(creational.id), s"Should contain the Creational category Given $rootWithoutClassScope")
    assert(!rootWithoutClassScope.contains(structural.id), s"Should contain the Structural category Given $rootWithoutClassScope")
    assert(!rootWithoutClassScope.contains(behavioural.id), s"Should contain the Behavioural category Given $rootWithoutClassScope")
    assert(!rootWithoutClassScope.contains(classScope.id), s"Should not contain the Class Scope category.  Given $rootWithoutClassScope")
    assert(rootWithoutClassScope.contains(objectScope.id), s"Should contain the Object Scope category.  Given $rootWithoutClassScope")
  }
}
