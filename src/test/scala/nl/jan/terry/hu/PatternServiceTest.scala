package nl.jan.terry.hu
import spray.json._
import scala.concurrent.duration._
import nl.jan.terry.hu.domain.{InitialModel, Root}
import nl.jan.terry.hu.util.{JsonMarshallers, Observable, Observer}
import nl.jan.terry.hu.spec.TestSpec
import akka.testkit.TestProbe
import nl.jan.terry.hu.service.PatternService
import nl.jan.terry.hu.repository.{Repository, EventSourcedRepository}

/**
 * Created by terryhendrix on 02/04/15.
 */
class PatternServiceTest extends TestSpec with JsonMarshallers
{
  "PatternService" should "initialize the model" in {
    service.getRoot
    Thread.sleep(10) // asynchronous code requires a bit of time :)
    assert(service.getRoot == InitialModel.root, "Should start with InitialModel.root")
  }

  it should "import the testing model" in {
    service.importJson(root.asJson())
    Thread.sleep(10) // asynchronous code requires a bit of time :)
    assert(service.getRoot == root, "The testing model should have been loaded")
  }

  it should "delete the factory pattern in" in {
    service.delete(factory.id)
    Thread.sleep(10)
    assert(!service.getRoot.contains(factory.id), "The factory method must be deleted")
  }

  it should "delete the creational category in" in {
    service.delete(creational.id)
    Thread.sleep(10)
    assert(!service.getRoot.contains(factory.id), "The factory pattern must be deleted")
    assert(!service.getRoot.contains(cor.id), "The Cor pattern must be deleted")
    assert(!service.getRoot.contains(creational.id), "The creational category must be deleted")
  }

  it should "delete the class scope category in" in {
    service.delete(classScope.id)
    Thread.sleep(10)
    assert(!service.getRoot.contains(factory.id), "The factory pattern must be deleted")
    assert(!service.getRoot.contains(cor.id), "The Cor pattern must be deleted")
    assert(!service.getRoot.contains(creational.id), "The creational category must be deleted")
    assert(!service.getRoot.contains(behavioural.id), "The behavioural category must be deleted")
    assert(!service.getRoot.contains(structural.id), "The Structural category must be deleted")
    assert(!service.getRoot.contains(classScope.id), "The class scope must be deleted")
    assert(service.getRoot.contains(objectScope.id), "The object scope should still be here")
  }

  it should "export-import the original root en re-import the previous model" in {
    val previousRoot = service.getRoot
    val export = service.exportJson()

    service.importJson(root.asJson())
    Thread.sleep(10)
    assert(service.getRoot == root, "Should import correctly")

    service.importJson(export.body)
    Thread.sleep(10)
    assert(service.getRoot == previousRoot, "Model should be restored")
  }
}
