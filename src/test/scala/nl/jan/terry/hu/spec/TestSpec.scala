package nl.jan.terry.hu.spec

import akka.actor.ActorSystem
import org.scalatest.{Matchers, BeforeAndAfterAll, FlatSpecLike}
import akka.testkit.TestKit
import java.util.UUID
import nl.jan.terry.hu.domain.{Root, Category, Pattern}
import nl.jan.terry.hu.util.{Observable, Convert}
import nl.jan.terry.hu.repository.EventSourcedRepository
import nl.jan.terry.hu.repository.Repository
import nl.jan.terry.hu.service.PatternService

/**
 * Created by terryhendrix on 02/04/15.
 */
class TestSpec extends TestKit(ActorSystem("Patterns")) with FlatSpecLike with BeforeAndAfterAll with Matchers
{
  import Convert.uuidToString
  implicit val log = system.log
  val repo:Repository with Observable[Root] = EventSourcedRepository(system)
  val service = new PatternService(repo)

  val cor = Pattern(UUID.randomUUID(), "Chain of responsibility", "many handlers", "Cannot create reference", "a","b",None,"some consequences")
  val factory = Pattern(UUID.randomUUID(), "Chain of responsibility", "many handlers", "Cannot create reference", "a","b",None,"some consequences")

  val creational = Category(UUID.randomUUID(), "Creational", "Creational class patterns", patterns = cor :: factory :: Nil)
  val structural = Category(UUID.randomUUID(), "Structural", "Structural class patterns")
  val behavioural = Category(UUID.randomUUID(), "Behavioral", "Behavioral class patterns")

  val classScope = Category(UUID.randomUUID(), "Class scope", "Category for class patterns", categories = creational :: behavioural :: structural :: Nil)
  val objectScope = Category(UUID.randomUUID(), "Object scope", "Category for object patterns")

  val root = Root(UUID.randomUUID(), objectScope :: classScope :: Nil)

  override def afterAll() = {
    system.shutdown()
    system.awaitTermination()
  }
}
