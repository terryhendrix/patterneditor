import sbt.Keys._

logLevel := Level.Info

organization := "nl.jan.terry"

name := "pattern-editor"

version := "1.0.0-SNAPSHOT"

scalaVersion := "2.11.4"

credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")

libraryDependencies ++=
  {
    val akkaV = "2.3.6"
    val sprayV = "1.3.1"
    val jsonV = "1.2.6"
    Seq(
      "org.scala-lang"           % "scala-library"                  % scalaVersion.value,
      "com.typesafe.akka"       %% "akka-actor"                     % akkaV,
      "com.typesafe.akka"       %% "akka-persistence-experimental"  % akkaV,
      "io.spray"                %% "spray-http"                     % sprayV,
      "io.spray"                %% "spray-routing"                  % sprayV,
      "io.spray"                %% "spray-util"                     % sprayV,
      "io.spray"                %% "spray-io"                       % sprayV,
      "io.spray"                %% "spray-can"                      % sprayV,
      "io.spray"                %% "spray-json"                     % jsonV,
      "com.typesafe.akka"       %% "akka-testkit"                   % akkaV   % "test",
      "io.spray"                %% "spray-testkit"                  % sprayV  % "test",
      //"nl.bluesoft"             %% "akka-persistence-inmemory"      % "1.0.0" % "test",
      "org.scalatest"           %% "scalatest"                      % "2.1.4" % "test"
    )
  }

autoCompilerPlugins := true

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

publishMavenStyle := true

publishArtifact in Test := false

fork := true

fork in run := true

fork in test := true

(parallelExecution in test) := false